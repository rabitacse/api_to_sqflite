import 'package:flutter/material.dart';


 class NoConnectivityView extends StatelessWidget {
   @override
   Widget build(BuildContext context) {
     return Scaffold(
       appBar: AppBar(
         title: Text('Contact App'),

       ),
       body: Container(
        //alignment: Alignment(10, 20),
         child: Center(
           child: Text(
             "No Internet Connection",
             textAlign: TextAlign.center,
             style: TextStyle(
                 color: Colors.red,
                 fontSize: 17,
                 fontWeight: FontWeight.bold
             ),

           ),

         ),

       ),

     );



   }
 }
