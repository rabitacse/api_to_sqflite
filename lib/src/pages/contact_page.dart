import 'package:flutter/material.dart';
import 'package:rabita/src/models/contact_model.dart';
import 'package:rabita/src/models/discipline_model.dart';
import 'package:rabita/src/providers/contact_api_provider.dart';
import 'package:rabita/src/providers/db_provider.dart';


class ContactPage extends StatefulWidget {

  final Discipline  discipline;//if you have multiple values add here

  ContactPage(this.discipline, {Key key}): super(key: key);//add also..example this.abc,this...
  @override
  State<StatefulWidget> createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {

  var  isLoading = false;
  void initState() {

    super.initState();
    print('Users');
    _loadFromApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.discipline.name),
        centerTitle: true,
        actions: <Widget>[
          Container(
            padding: EdgeInsets.only(right: 10.0),
            child: IconButton(
              icon: Icon(Icons.settings_input_antenna),
              onPressed: () async {
                await _loadFromApi();
              },
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 10.0),
            child: IconButton(
              icon: Icon(Icons.delete),
              onPressed: () async {
                await _deleteUsers();
              },
            ),
          ),

        ],
      ),
      body: isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          : _buildContactListView(),
    );
  }

  _loadFromApi() async {
    setState(() {
      isLoading = true;
      print('true');
    });

    var apiProvider = ContactApiProvider();
    await apiProvider.getAllUsersByDisciplineId(widget.discipline.id);

    // wait for 2 seconds to simulate loading of data
    await Future.delayed(const Duration(seconds: 2));

    setState(() {
      isLoading = false;
      print('false');
    });
  }
  _deleteUsers() async {
    setState(() {
      isLoading = true;
    });

    await DBProvider.db.deleteAllUsers();

    // wait for 1 second to simulate loading of data
    await Future.delayed(const Duration(seconds: 1));

    setState(() {
      isLoading = false;
    });

    print('All users deleted');
  }

  _buildContactListView() {
    return FutureBuilder(
      future: DBProvider.db.getAllUsersByDisciplineId(widget.discipline.id),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.red,
            ),
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: Text(
                  "${index + 1}",
                  style: TextStyle(fontSize: 20.0),
                ),
                title: Text(snapshot.data[index].name),

                onTap:(){


                },


              );
            },
          );
        }
      },
    );
  }

}

