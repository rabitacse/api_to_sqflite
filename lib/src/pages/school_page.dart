import 'package:rabita/src/pages/discipline_page.dart';
import 'package:rabita/src/providers/db_provider.dart';
import 'package:rabita/src/providers/school_api_provider.dart';
import 'package:flutter/material.dart';



class SchoolPage extends StatefulWidget {
  @override
  _SchoolPageState createState() => _SchoolPageState();
}

class _SchoolPageState extends State<SchoolPage> {
  var isLoading = false;



 @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadFromApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Api to sqlite'),
        centerTitle: true,
        actions: <Widget>[
          Container(
            padding: EdgeInsets.only(right: 10.0),
            child: IconButton(
              icon: Icon(Icons.settings_input_antenna),
              onPressed: () async {
                await _loadFromApi();
              },
            ),
          ),

         Container(
            padding: EdgeInsets.only(right: 10.0),
            child: IconButton(
              icon: Icon(Icons.remove),
              onPressed: () async {
                await _deleteData();
              },
            ),
          ),


        ],
      ),
      body: isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          : _buildSchoolListView(),
    );
  }



  _loadFromApi() async {
    setState(() {
      isLoading = true;
      print('true');
    });

    var apiProvider = SchoolApiProvider();
    await apiProvider.getAllSchools();

    // wait for 2 seconds to simulate loading of data
    await Future.delayed(const Duration(seconds: 2));

    setState(() {
      isLoading = false;
      print('false');
    });
  }

  
  _deleteData() async {
    setState(() {
      isLoading = true;
    });

    await DBProvider.db.deleteAllSchools();

    // wait for 1 second to simulate loading of data
    await Future.delayed(const Duration(seconds: 1));

    setState(() {
      isLoading = false;
    });

    print('All employees deleted');
  }
  
  
  _buildSchoolListView() {
    return FutureBuilder(
      future: DBProvider.db.getAllSchools(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.red,
            ),
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
//                leading: Text(
//                  "${index + 1}",
//                 // style: TextStyle(fontSize: 20.0),
//                ),
                leading: CircleAvatar(
                child: Text(snapshot.data[index].name[0],
                  style:TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20
                  ),
                ),
                ),
                title: Text(snapshot.data[index].name),

                onTap:(){
                   Navigator.push(context, new MaterialPageRoute(builder:
                        (context)=>DisciplinePage(snapshot.data[index]))
                   );
                   },
              );
            },
          );
        }
      },
    );
  }




}
