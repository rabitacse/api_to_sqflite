import 'package:flutter/material.dart';
import 'package:rabita/src/models/contact_model.dart';
import 'package:rabita/src/models/school_model.dart';
import 'package:rabita/src/providers/discipline_api_provider.dart';
import 'package:rabita/src/providers/db_provider.dart';
import 'package:rabita/src/pages/contact_page.dart';


class DisciplinePage extends StatefulWidget {

  final School school;//if you have multiple values add here
  DisciplinePage(this.school, {Key key}): super(key: key);//add also..example this.abc,this...

  @override
  State<StatefulWidget> createState() => _DisciplinePageState();
}

class _DisciplinePageState extends State<DisciplinePage> {

  var  isLoading = false;
  void initState() {

    super.initState();
    print('discipline');
    _loadFromApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.school.name),
        centerTitle: true,
        actions: <Widget>[
          Container(
            padding: EdgeInsets.only(right: 10.0),
            child: IconButton(
              icon: Icon(Icons.settings_input_antenna),
              onPressed: () async {
                await _loadFromApi();
              },
            ),
          ),
        ],
      ),
      body: isLoading
          ? Center(
        child: CircularProgressIndicator(),
      )
          : _buildDisciplineListView(),
    );
  }

  _loadFromApi() async {
    setState(() {
      isLoading = true;
      print('true');
    });

    var apiProvider = DisciplineApiProvider();
    await apiProvider.getAllDisciplinesBySchoolId(widget.school.id);

    // wait for 2 seconds to simulate loading of data
    await Future.delayed(const Duration(seconds: 2));

    setState(() {
      isLoading = false;
      print('false');
    });
  }


  _buildDisciplineListView() {
    return FutureBuilder(
      future: DBProvider.db.getAllDisciplinesBySchoolId(widget.school.id),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.red,
            ),
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
//                leading: Text(
//                  "${index + 1}",
//                 // style: TextStyle(fontSize: 20.0),
//                ),
                leading: CircleAvatar(
                  child: Text(snapshot.data[index].name[0],
                    style:TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                    ),
                  ),
                ),
                title: Text(snapshot.data[index].name),

                onTap:(){
                  Navigator.push(context, new MaterialPageRoute(builder:
                      (context)=>ContactPage(snapshot.data[index]))
                  );
                },
              );
            },
          );
        }
      },
    );
  }

}

