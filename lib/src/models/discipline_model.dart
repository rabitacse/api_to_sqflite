class Discipline{
  int id;
  int schoolId;
  String name;

  Discipline({this.id, this.schoolId, this.name});

  Discipline.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    schoolId = json['school_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['school_id'] = this.schoolId;
    data['name'] = this.name;
    return data;
  }
}