import 'dart:convert';

List<Contact> employeeFromJson(String str) =>
    List<Contact>.from(json.decode(str).map((x) => Contact.fromJson(x)));

String employeeToJson(List<Contact> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Contact {
  int id;
  String email;
  String name;
  String mobile;
  int disciplineId;
  int schoolId;

  Contact(
      {this.id,
        this.email,
        this.name,
        this.mobile,
        this.disciplineId,
        this.schoolId});

  Contact.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    name = json['name'];
    mobile = json['mobile'];
    disciplineId = json['discipline_id'];
    schoolId = json['school_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['name'] = this.name;
    data['mobile'] = this.mobile;
    data['discipline_id'] = this.disciplineId;
    data['school_id'] = this.schoolId;
    return data;
  }
}