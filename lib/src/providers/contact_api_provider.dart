import 'package:rabita/src/models/discipline_model.dart';
import 'package:rabita/src/models/contact_model.dart';
import 'package:rabita/src/providers/db_provider.dart';
import 'package:dio/dio.dart';

class ContactApiProvider {
  Future<List<Contact>> getAllUsersByDisciplineId(int id) async {
    var url = "http://contactdroid.000webhostapp.com/users";
    Response response = await Dio().get(url);

    return (response.data as List).map((contact) {
      print('Inserting $contact');
      DBProvider.db.createUsers(Contact.fromJson(contact));
    }).toList();
  }
}