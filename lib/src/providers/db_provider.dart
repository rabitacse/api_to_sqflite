import 'dart:io';
import 'package:rabita/src/models/contact_model.dart';
import 'package:rabita/src/models/school_model.dart';
import 'package:rabita/src/models/discipline_model.dart';
import 'package:rabita/src/models/discipline_model.dart';
import 'package:path/path.dart';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();
  final String columnId="school_id";

  DBProvider._();

  Future<Database> get database async {
    // If database exists, return database
    if (_database != null) return _database;

    // If database don't exists, create one
    _database = await initDB();

    return _database;
  }

   // Create the database and the Employee table
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'school2.db');

    return await openDatabase(path, version: 2, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE School('
          'id INTEGER PRIMARY KEY,'
          'name TEXT'
          ')');
      await db.execute('CREATE TABLE Discipline('
          'id INTEGER PRIMARY KEY,'
          'school_id  INTEGER,'
          'name TEXT'
          ')');

      await db.execute('CREATE TABLE Users('
          'id INTEGER PRIMARY KEY,'
          'email  TEXT,'
          'name  TEXT,'
          'mobile  TEXT,'
          'designation  TEXT,'
          'discipline_id  INTEGER,'
          'school_id  INTEGER'

          ')');



    });
  }


  // Insert employee on database
  createEmployee(Contact newEmployee) async {
    await deleteAllEmployees();
    final db = await database;
    final res = await db.insert('Employee', newEmployee.toJson());

    return res;
  }

  createSchool(School newSchool) async {
   // await deleteAllSchools();
    final db = await database;
    final res = await db.insert('School', newSchool.toJson());

    return res;
  }

  createDiscipline(Discipline newDiscipline) async {
    //await deleteAllSchools();
    final db = await database;
    final res = await db.insert('Discipline', newDiscipline.toJson());

    return res;
  }

  createUsers(Contact newContact) async {

    final db = await database;
    final res = await db.insert('Users', newContact.toJson());

    return res;
  }



  // Delete all employees
  Future<int> deleteAllEmployees() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Employee');

    return res;
  }

 // Delete all employees
  Future<int> deleteAllSchools() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM School');

    return res;
  }


  Future<int> deleteAllUsers() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Users');

    return res;
  }

  Future<List<Contact>> getAllEmployees() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM EMPLOYEE");

    List<Contact> list =
    res.isNotEmpty ? res.map((c) => Contact.fromJson(c)).toList() : [];

    return list;
  }

  Future<List<School>> getAllSchools() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM SCHOOL");

    List<School> list =
    res.isNotEmpty ? res.map((c) => School.fromJson(c)).toList() : [];

    return list;
  }





  Future<List<Discipline>> getAllDisciplinesBySchoolId(int id) async {
    
    final db=await database;
    final res=await db.rawQuery("SELECT * FROM DISCIPLINE WHERE $columnId=$id");

    List<Discipline> list =
    res.isNotEmpty ? res.map((c) => Discipline.fromJson(c)).toList() : [];

    return list;
    
  }


  Future<List<Contact>> getAllUsersByDisciplineId(int id) async {

    final db=await database;
    final res=await db.rawQuery("SELECT * FROM USERS WHERE discipline_id=$id");

    List<Contact> list =
    res.isNotEmpty ? res.map((c) => Contact.fromJson(c)).toList() : [];

    return list;

  }


}