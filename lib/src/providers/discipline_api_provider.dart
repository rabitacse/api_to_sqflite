import 'package:rabita/src/models/discipline_model.dart';
import 'package:rabita/src/models/school_model.dart';
import 'package:rabita/src/providers/db_provider.dart';
import 'package:dio/dio.dart';

class DisciplineApiProvider {
  Future<List<Discipline>> getAllDisciplinesBySchoolId(int id) async {
    var url = "http://contactdroid.000webhostapp.com/discipline";
    Response response = await Dio().get(url);

    return (response.data as List).map((discipline) {
      print('Inserting $discipline');
      DBProvider.db.createDiscipline(Discipline.fromJson(discipline));
    }).toList();
  }
}