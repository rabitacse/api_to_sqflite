import 'package:rabita/src/models/school_model.dart';
import 'package:rabita/src/providers/db_provider.dart';
import 'package:dio/dio.dart';

class SchoolApiProvider {
  Future<List<School>> getAllSchools() async {
    var url = "http://contactdroid.000webhostapp.com/schools";
    Response response = await Dio().get(url);

    return (response.data as List).map((school) {
      print('Inserting $school');
      DBProvider.db.createSchool(School.fromJson(school));
    }).toList();
  }
}